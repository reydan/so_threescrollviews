//
//  ViewController.m
//  ThreeScrollviews
//
//  Created by Andrei Stanescu on 2/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "ZoomScrollView.h"

@implementation ViewController

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    // Setup our horizontal scrollview
    hscroll = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    //hscroll.pagingEnabled = true;
    hscroll.delegate = self;
    //hscroll.backgroundColor = [UIColor magentaColor];
    hscroll.minimumZoomScale = 1.0f;
    hscroll.maximumZoomScale = 1.0f;
    hscroll.zoomScale = 1.0f;
    hscroll.bouncesZoom = false;
    hscroll.alwaysBounceVertical = false;
    hscroll.delaysContentTouches = false;

    int BORDER_SIZE = 0;
    
    // Now add 3 vertical scrollviews as content to the horizontal scrollview
    int i = 0;
    for (i = 0; i < 3; i++)
    {
        
        UIView* vcontainer = [[UIView alloc] initWithFrame:CGRectMake(BORDER_SIZE + i * self.view.bounds.size.width, BORDER_SIZE + 0, -BORDER_SIZE*2 + self.view.bounds.size.width, -BORDER_SIZE*2 + self.view.bounds.size.height)];
        
        UIScrollView* vscroll = [[UIScrollView alloc] initWithFrame:vcontainer.bounds];
        vscroll.pagingEnabled = true;
        vscroll.delegate = self;
        vscroll.minimumZoomScale = 1.0f;
        vscroll.maximumZoomScale = 1.0f;
        vscroll.zoomScale = 1.0f;
        vscroll.bouncesZoom = false;
        vscroll.backgroundColor = [UIColor redColor];
        vscroll.bounces = true;
        vscroll.alwaysBounceHorizontal = true;
        vscroll.alwaysBounceVertical = true;
        
        
        // For each of the vertical scrolls, add some dummy content 
        int j = 0;
        for (j = 0; j < 5; j++)
        {
            UIView* container = [[UIView alloc] initWithFrame:CGRectMake(BORDER_SIZE, BORDER_SIZE + j * vscroll.bounds.size.height, -BORDER_SIZE*2 + vscroll.bounds.size.width, -BORDER_SIZE*2 + vscroll.bounds.size.height)];

            // The image is incapsulated as a scrollview
            ZoomScrollView* imgscroll = [[ZoomScrollView alloc] initWithFrame:container.bounds];
            imgscroll.minimumZoomScale = 1.0f;
            imgscroll.maximumZoomScale = 3.0f;
            imgscroll.zoomScale = 1.0f;
            imgscroll.bouncesZoom = true;
            imgscroll.bounces = true;
            imgscroll.tag = 10;
            imgscroll.delegate = self;
            imgscroll.backgroundColor = [UIColor blueColor];
            imgscroll.alwaysBounceHorizontal = false;

            UIImageView* imgview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgscroll.bounds.size.width, imgscroll.bounds.size.height)];
            imgview.image = [UIImage imageNamed:@"buddha.jpg"];
            imgview.contentMode = UIViewContentModeScaleToFill;
            imgview.backgroundColor = [UIColor blackColor];
                                                                                 
                                                                                 
            imgscroll.contentSize = imgview.bounds.size;            
            
            [imgscroll addSubview:imgview];
            [container addSubview:imgscroll];
            
            
            [vscroll addSubview:container];
        }
        
        vscroll.contentSize = CGSizeMake(vscroll.bounds.size.width, j * vscroll.bounds.size.height);
        
        [vcontainer addSubview:vscroll];

        [hscroll addSubview:vcontainer];        
    }
    
    hscroll.contentSize = CGSizeMake(i * self.view.bounds.size.width, self.view.bounds.size.height);
    
    [self.view addSubview:hscroll];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - UIScrollViewDelegate
- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    // Test to see which scrollview generated the message
    if (scrollView == hscroll)
    {
        // It is the main horizontal scrollview
        return nil;
    }
    else if ([hscroll.subviews containsObject:scrollView])
    {
        // It is one of the vertical scrollers
        //return [scrollView.subviews objectAtIndex:0];
        return nil;
    }
    else if (scrollView.tag == 10)
    {
        // It is one of the content image scroll views
        return [scrollView.subviews objectAtIndex:0];
    }
        
    return nil;
}
@end
